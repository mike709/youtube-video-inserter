from input import video, thumbnail, text
from youtube import upload_video

class App:
    def main(self):
        input_link = input('Give us the link to thumbnail: ')
        input_title = input('Write title: ')
        input_description = input('Write description: ')
        input_tags = input("Write tags and separate words by ',':  ")
        get_thumbnail = thumbnail.ThumbnailInput(input_link).get_thumbnail()
        get_video = video.VideoInput().get_video()
        get_text = text.TextInput(input_title, input_description, input_tags).get_all_text()
        tupleed_result = (get_video, get_thumbnail, get_text)
        upload_video.YoutubeLoader(get_video, get_text["title"], get_text["description"], get_text["tags"])
        print(tupleed_result)


if __name__ == "__main__":
    app = App()
    app.main()