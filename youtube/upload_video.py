import datetime
from google_connect import google
from googleapiclient.http import MediaFileUpload

class YoutubeLoader:
    def __init__(self, video, title, description, tags) -> None:
        self.video = video
        self.title = title
        self.description = description
        self.tags = tags

    def main(self):
        CLIENT_SECRET_FILE = 'client_secrets.json'
        API_NAME = 'youtube'
        API_VERSION = 'v3'
        SCOPES = ['https://www.googleapis.com/auth/youtube.upload']

        service = google.Google(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES).create_service()

        upload_date_time = datetime.datetime.now().isoformat() + '.000Z'

        request_body = {
            'snippet': {
                'categoryId': 23,
                'title': self.title,
                'description': self.description,
                'tags': self.tags
            },
            'status': {
                'privacyStatus': 'private',
                'publishAt': upload_date_time,
                'selfDeclaredMadeForKids': False, 
            },
            'notifySubscribers': False
        }

        mediaFile = MediaFileUpload('../video/' + self.video["video_file"])

        response_upload = service.videos().insert(
          part='snippet,status',
           body=request_body,
           media_body=mediaFile
        ).execute()


        service.thumbnails().set(
            videoId=response_upload.get('id'),
            media_body=MediaFileUpload('../thumbnail/test.jpeg')
        ).execute()