# Youtube video loader

## Description

You will load a video to youtube web service using this script.
You just needed to enter link to the thumbnail, video's title and write a description of video and add video to "video" folder.
Create folder "video" to root of app if it is not exists.

## Installation

Before you start you have to create client_secrets.json file:

```bash
{
  "web": {
    "client_id": "[[INSERT CLIENT ID HERE]]",
    "client_secret": "[[INSERT CLIENT SECRET HERE]]",
    "redirect_uris": [],
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token"
  }
}
```

... and verify you account forwarding to the link:

```bash
    https://www.youtube.com/verify
```

On Youtube API you can find out how to get CLIENT ID and CLIENT SECRET.

### Dependencies
```bash
    pip install -r requirements.txt
```

## Start app
In terminal enter:
```bash
    python main.py
```
and fill all neccessary requirements