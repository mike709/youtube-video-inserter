import requests
import os
from io import BytesIO
from PIL import Image

class ThumbnailInput:
    size = 512, 512 
    def __init__(self, link) -> None:
        self.link = link

    def get_thumbnail(self):
        if not os.path.exists('thumbnail'):
            os.mkdir('thumbnail')
        response = requests.get(self.link)
        with Image.open(BytesIO(response.content)) as img:
            img.thumbnail(self.size)
            img.save("./thumbnail/test." + img.format.lower(), "JPEG")
            return img
        
