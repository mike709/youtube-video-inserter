import os

class VideoInput:
    def __init__(self) -> None:
        pass

    def get_video(self):
        dir = os.listdir(os.getcwd() + '/video')

        if(len(dir) != 0):
            for file in os.listdir('video'):
                if file.endswith(('mp4', 'MP4')):
                    file_name = file.split('.')[0]
                    return {
                        "file_name": file_name,
                        "video_file": file
                    }
                else:
                    print("Format is not correct. Use '.mp4' format, please")