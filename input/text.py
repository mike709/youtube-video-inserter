class TextInput:
    def __init__(self, title, description, tags) -> None:
        self.title = title
        self.description = description
        self.tags = tags

    def get_all_text(self):
        tags = self.tags.split(',')
        return {
            "title": self.title,
            "description": self.description,
            "tags": tags
        }